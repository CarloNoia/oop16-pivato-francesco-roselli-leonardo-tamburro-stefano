package model.enumerations;
	
public enum ExerciseType {
	
	SHOULDERS,
	TRICEPS,
	BICEPS,
	DORSALS,
	ABDOMINALS,
	LEGS;
}

